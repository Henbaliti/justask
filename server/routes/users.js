const express = require("express");
const router = express.Router();
const { User } = require("../models/user");
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

//Get all users
router.get("/", async (req, res) => {
  const userList = await User.find().populate("reviews");

  if (!userList) {
    res.status(500).json({ success: false });
  }
  res.send(userList);
});

//Get user by id
router.get("/:id", async (req, res) => {
  const user = await User.findById(req.params.id); //.select("artibute1 artibute2 -artibute3")

  if (!user) {
    res.status(500).json({ success: false, message: "The user not found" });
  }
  res.status(200).send(user);
});

//Create user
router.post("/addUser", (req, res) => {
  //validate all required data is passed
  if (
    !req.body.name ||
    !req.body.country ||
    !req.body.password ||
    !req.body.email
  ) {
    return res
      .status(404)
      .send("The user can not be created - missing parameters");
  }

  //create the user
  const user = new User({
    name: req.body.name,
    passwordHash: bcrypt.hashSync(req.body.password, 10),
    singUpTime: Date.now(),
    email: req.body.email,
    country: req.body.country,
    image: req.body.image,
  });

  //save the user to db
  user.save();

  if (!user)
    return res.status(404).send("The user can not be created - DB error");
  return res.send(user);
});

//Login
router.post("/login", async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  const secret = process.env.secret;

  //check if the mail exist in db 
  if (!user) return res.status(400).send("The user is not found");

  //check if the password is correct
  if (user && bcrypt.compareSync(req.body.password, user.passwordHash)) {
    const token = jwt.sign(
      {
        userId: user.id,
      },
      secret,
      {
        expiresIn: "1d", //expires time - 1h, 1d , 1w ,1m
      }
    );
    res.status(200).send({user: user.email , token: token}) 
  } else {
    res.status(400).send("Password is wrong");
  }
});

//Update user
router.put("/:id", async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).send("User not exist");
  }
  const user = await User.findByIdAndUpdate(
    req.params.id,
    {
      fullName: req.body.fullName,
      userName: req.body.userName,
      password: req.body.password,
      singUpTime: req.body.singUpTime,
      email: req.body.email,
      reviews: req.body.reviews,
    },
    { new: true }
  );
  if (!user) return res.status(404).send("The user can not be Updated");
  return res.send(user);
});

//Delete user
router.delete("/:id", async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).json({
      success: false,
      message: "Exeption occured.\nPlease try again later",
    });
  }

  const user = await User.findByIdAndRemove(req.params.id);
  if (!user) {
    return res
      .status(404)
      .json({ success: false, message: "Cannot find the user" });
  }

  return res
    .status(200)
    .json({ success: true, message: "The user has been deleted" });
});

//Get the count of the users
router.get("/get/count", async (req, res) => {
  const countUsers = await User.countDocuments((count) => count);

  if (!countUsers) {
    res.status(500).json({ success: false });
  }
  res.status(200).send({ countUsers: countUsers });
});

module.exports = router;
