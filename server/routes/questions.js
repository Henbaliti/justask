const express = require("express");
const router = express.Router();
const { Question } = require("../models/question");
const checkIfTagExist = require("../helper/checkIfTagExist");
const mongoose = require("mongoose");
const { Tag } = require("../models/tag");
const multer = require("multer");

const FILE_TYPE_MAP = {
  "image/png": "png",
  "image/jpeg": "jpeg",
  "image/jpg": "jpg",
};

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const isValid = FILE_TYPE_MAP[file.mimetype];
    let uploadError = new Error("invalid image type");

    if (isValid) {
      uploadError = null;
    }
    cb(uploadError, "public/uploads");
  },
  filename: function (req, file, cb) {
    const fileName = file.originalname.split(" ").join("-");
    const extension = FILE_TYPE_MAP[file.mimetype];
    cb(null, `${fileName}-${Date.now()}.${extension}`);
  },
});

const uploadOptions = multer({ storage: storage });

//Get all questions
router.get("/", async (req, res) => {
  const questionsList = await Question.find()
    .populate("tags", "author")
    .sort({ postedTime: -1 }); // newest to oldest

  if (!questionsList) {
    res.status(500).json({ success: false });
  }
  res.send(questionsList);
});

//Get question by id
router.get("/:id", async (req, res) => {
  const question = await Question.findById(req.params.id); //.select("artibute1 artibute2 -artibute3")

  if (!question) {
    res.status(500).json({ success: false, message: "The question not found" });
  }
  res.status(200).send(question);
});

//Create question
router.post("/addQuestion", uploadOptions.array('images', 10),  async (req, res) => {
  //validate all required data is passed
  if (
    !req.body.title ||
    !req.body.description ||
    !req.body.author ||
    !req.body.price
  ) {
    return res
      .status(404)
      .send("The user can not be created - missing parameters");
  }

  const files = req.files
  let imagesPaths = [];
  const basePath = `${req.protocol}://${req.get('host')}/public/uploads/`;

  if(files) {
    files.map(file =>{
        imagesPaths.push(`${basePath}${file.filename}`);
    })
 }


  //Check if the tag is exist in DB. in case isn't-create
  const allTags = Promise.all(
    req.body.tags.map(async (tagName) => {
      let newTag = await checkIfTagExist(tagName);
      if (newTag == null) {
        const createdTag = new Tag({ type: tagName });
        await createdTag.save();
        return createdTag;
      }
      return newTag;
    })
  );

  const allTagsResolved = await allTags;

  //create the question
  const question = new Question({
    title: req.body.title,
    description: req.body.description,
    postedTime: Date.now(),
    author: req.body.author,
    price: req.body.price,
    images: imagesPaths,
    tags: allTagsResolved,
  });

  //save the question to db
  question.save();

  if (!question)
    return res.status(404).send("The question can not be created - DB error");
  return res.send(question);
});

//Update exist question
router.put("/:id", async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).send("Question not exist");
  }
  const question = await Question.findByIdAndUpdate(
    req.params.id,
    {
      title: req.body.title,
      description: req.body.description,
      postedTime: Date.now(),
      author: req.body.author,
      price: req.body.price,
      images: req.body.images,
      tags: tagsArray,
    },
    { new: true }
  );
  if (!question) return res.status(404).send("The question can not be Updated");
  return res.send(question);
});

//Delete question
router.delete("/:id", async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).json({
      success: false,
      message: "Exeption occured.\nPlease try again later",
    });
  }

  const question = await Question.findByIdAndRemove(req.params.id);
  if (!question) {
    return res
      .status(404)
      .json({ success: false, message: "Cannot find the question" });
  }

  return res
    .status(200)
    .json({ success: true, message: "The question has been deleted" });
});

//Get the count of the question
router.get("/get/count", async (req, res) => {
  const countQuestion = await Question.countDocuments((count) => count);

  if (!countQuestion) {
    res.status(500).json({ success: false });
  }
  res.status(200).send({ countQuestion: countQuestion });
});

router.put(
'/gallery-images/:id', 
  uploadOptions.array('images', 10), 
  async (req, res)=> {
      if(!mongoose.isValidObjectId(req.params.id)) {
          return res.status(400).send('Invalid Product Id')
       }
       const files = req.files
       let imagesPaths = [];
       const basePath = `${req.protocol}://${req.get('host')}/public/uploads/`;

       if(files) {
          files.map(file =>{
              imagesPaths.push(`${basePath}${file.filename}`);
          })
       }

       const product = await Product.findByIdAndUpdate(
          req.params.id,
          {
              images: imagesPaths
          },
          { new: true}
      )

      if(!product)
          return res.status(500).send('the gallery cannot be updated!')

      res.send(product);
  }
)

module.exports = router;
