const express = require("express");
const router = express.Router();
const { Review } = require("../models/review");
const mongoose = require("mongoose");

//Get all reviews
router.get("/", async (req, res) => {
  const reviewList = await Question.find().populate("authorUser","targetUser");

  if (!reviewList) {
    res.status(500).json({ success: false });
  }
  res.send(reviewList);
});

//Get user by id
router.get("/:id", async (req, res) => {
    const review = await Review.findById(req.params.id); //.select("artibute1 artibute2 -artibute3")
  
    if (!review) {
      res.status(500).json({ success: false, message: "The review not found" });
    }
    res.status(200).send(review);
});

//Create review
router.post("/addReview", (req, res) => {
  //validate all required data is passed
  if (
    !req.body.title ||
    !req.body.description ||
    !req.body.authorUser ||
    !req.body.targetUser ||
    !req.body.rating
  ) {
    return res
      .status(404)
      .send("The review can not be created - missing parameters");
  }

  //create the review
  const review = new Review({
    title: req.body.title,
    description: req.body.description,
    authorUser: req.body.authorUser,
    targetUser: req.body.targetUser,
    rating: req.body.rating,
    published: Date.now(),
  });

  //save the review to DB
  review = review.save();

  if (!review) return res.status(404).send("The review can not be created - DB error");
  return res.send(review);

});

//Update review
router.put("/:id", async (req, res) => {
    if (!mongoose.isValidObjectId(req.params.id)) {
      return res.status(400).send("Review not exist");
    }
    const review = await Review.findByIdAndUpdate(
      req.params.id,
      {
        title: req.body.title,
        description: req.body.description,
        authorUser: req.body.authorUser,
        targetUser: req.body.targetUser,
        rating: req.body.rating,
      },
      { new: true }
    );
    if (!review) return res.status(404).send("The review can not be Updated");
    return res.send(review);
});

//Delete review
router.delete("/:id", async (req, res) => {
    if (!mongoose.isValidObjectId(req.params.id)) {
      return res.status(400).json({
        success: false,
        message: "Exeption occured.\nPlease try again later",
      });
    }
  
    const review = await User.findByIdAndRemove(req.params.id);
    if (!review) {
      return res
        .status(404)
        .json({ success: false, message: "Cannot find the review" });
    }
  
    return res
      .status(200)
      .json({ success: true, message: "The review has been deleted" });
});

//Get the count of the reviews
router.get("/get/count", async (req, res) => {
    const countReviews = await Review.countDocuments((count) => count);
  
    if (!countReviews) {
      res.status(500).json({ success: false });
    }
    res.status(200).send({ countReviews: countReviews });
});
  

module.exports = router;
