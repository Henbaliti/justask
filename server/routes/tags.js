const express = require("express");
const router = express.Router();
const { Tag } = require("../models/tag");
const mongoose = require("mongoose");
const checkIfTagExist = require("../helper/checkIfTagExist");

//Get all tags
router.get("/", async (req, res) => {
  const tagList = await Tag.find();

  if (!tagList) {
    res.status(500).json({ success: false });
  }
  res.send(tagList);
});

//Get tag by id
router.get("/:id", async (req, res) => {
  const tag = await Tag.findById(req.params.id); //.select("artibute1 artibute2 -artibute3")

  if (!tag) {
    res.status(500).json({ success: false, message: "The tag not found" });
  }
  res.status(200).send(tag);
});

//Get tag by name
router.get("/", async (req, res) => {
  const tag = await Tag.findOne({ type: req.body.type });

  if (!tag) {
    res.status(500).json({ success: false, message: "The tag not found" });
  }
  res.status(200).send(tag);
});

//Create tag
router.post("/addTag", async (req, res) => {
  if (!req.body.type) {
    return res.status(404).send("The Tag can not be created - missing type");
  }

  //Check if the tag exist in db
  let existTag = await checkIfTagExist(req.body.type);

  //If exist then return
  if (existTag !== null) return res.send(existTag.toObject());

  //Create new Tag
  var tag = new Tag({
    type: req.body.type,
  });

  //Save the tag to db
  tag.save();

  //Return the value
  if (!tag)
    return res.status(404).send("The tag can not be created - DB error");
  return res.send(tag);
});

//Update Tag
router.put("/:id", async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).send("Tag not exist");
  }
  const tag = await Tag.findByIdAndUpdate(
    req.params.id,
    {
      type: req.body.type,
    },
    { new: true }
  );
  if (!tag) return res.status(404).send("The tag can not be Updated");
  return res.send(tag);
});

//Delete tag
router.delete("/:id", async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).json({
      success: false,
      message: "Exeption occured.\nPlease try again later",
    });
  }

  const tag = await Tag.findByIdAndRemove(req.params.id);
  if (!tag) {
    return res
      .status(404)
      .json({ success: false, message: "Cannot find the tag" });
  }

  return res
    .status(200)
    .json({ success: true, message: "The tag has been deleted" });
});

//Get the count of the tags
router.get("/get/count", async (req, res) => {
  const countTags = await Tag.countDocuments((count) => count);

  if (!countTags) {
    res.status(500).json({ success: false });
  }
  res.status(200).send({ countTags: countTags });
});

module.exports = router;
