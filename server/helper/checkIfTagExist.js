const { Tag } = require("../models/tag");

module.exports = async function checkIfTagExist(tagName){

    return await Tag.findOneAndUpdate(
       { type:  tagName},
       {
         $inc: {
           quantity: 0.5, // Increment by one
         },
       },
       { new: true },
       (err, foundTag) => {
         if (!err)
           console.log("Tag already exist , increased quantity by 1 " + foundTag);
       }
     );
}