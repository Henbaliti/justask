const expressJwt = require('express-jwt');

function authJwt() {
    const secret = process.env.secret;
    const api = 'localhost:8080/';
    return expressJwt({
        secret,
        algorithms: ['HS256'],
        // isRevoked: isRevoked
    }).unless({
        path: [
            // {url: /\/public\/uploads(.*)/ , methods: ['GET', 'OPTIONS'] },//Allow client to get photo without being authrized
            // {url: /\/questions(.*)/ , methods: ['GET', 'OPTIONS'] },//Allow client to get question or a list of questions without being authrized
            // {url: /\/api\/users\/login/ , methods: ['POST', 'OPTIONS'] }, //TODO: CHECK
            // {url: /\/api\/users\/addUser/ , methods: ['POST', 'OPTIONS'] }, //TODO: CHECK
            // `${api}/users/login`, //TODO: CHECK
            // `${api}/users/addUser`, //TODO: CHECK
            'localhost:8080/questions/',
            '/questions'
        ]
    })
}

//check if the token is of admin user
async function isRevoked(req, payload, done) {
    if(!payload.isAdmin) {
        done(null, true)
    }
    done();
}

module.exports = authJwt