const mongoose = require("mongoose");

const tagSchema = mongoose.Schema({
  type: {
    type: String,
    required: true,
  },
  quantity: {
    type: Number,
    default: 1,
  },
});

tagSchema.set('toJSON', {
  virtuals: true,
});

tagSchema.virtual('id').get(function () {
  return this._id.toHexString();
});


exports.Tag = mongoose.model("Tag", tagSchema);
