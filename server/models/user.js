const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  passwordHash: {
    type: String,
    required: true,
  },
  country: {
    type: String,
    required: true,
  },
  singUpTime: {
    type: Date,
    required: true,
    default: Date.now,
  },
  image: {
    type: String,
    required: false,
  },
  reviews: [
    {
      type: mongoose.Schema.Types.ObjectId, //represent the id of the related review
      ref: "Review",
      required: false,
    },
  ],
  isAdmin: {
    type: Boolean,
    required: false
  },
});

userSchema.set('toJSON', {
  virtuals: true,
});


exports.User = mongoose.model("User", userSchema);
