const mongoose = require("mongoose");

const questionSchema = mongoose.Schema({

  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  postedTime: {
    type: Date,
    required: true,
    default: Date.now,
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  price: {
    type: Number,
    required: true,
    min: 0,
  },
  images: [
    {
      type: String,
      required: false,
    },
  ],
  tags: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Tag",
      required: false,
    },
  ],
});

exports.Question = mongoose.model("Question", questionSchema);
