const express = require("express");
const app = express();
const bodyparser = require("body-parser");
const morgan = require("morgan");
const mongoose = require("mongoose");
const cors = require("cors");
const authJwt = require('./helper/jwt');
const errorHanler = require('./helper/error-handler');

//Enable CORS policy
app.use(cors());
app.options('*',cors());

//Access to .env file (use for save global variables - UNMODIFED)
require("dotenv/config");

//Middleware
app.use(bodyparser.json());
app.use(morgan("tiny"));
// app.use(authJwt);
app.use('/public/uploads', express.static(__dirname+'/public/uploads'));
app.use(errorHanler);

//Routes
const questions = require("./routes/questions");
const users = require("./routes/users");
const tags = require("./routes/tags");
const reviews = require("./routes/reviews");

//All the questions routes are relative to the first argument string
app.use("/questions", questions);
app.use("/users", users);
app.use("/tags", tags);
app.use("/reviews", reviews);

//DataBase connection
mongoose
  .connect(process.env.CONNECTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName: "JustAskDataBase",
  })
  .then(() => {
    console.log("DB Connection is ready...");
  })
  .catch((err) => {
    console.log(`@ Error occured: ${err}`);
  });

  //Server
  app.listen(8080, () => {
  console.log("Server is running on port 8080");
});
