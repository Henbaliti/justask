import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './css/forum.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Post from './components/pages/Post';
import List from './components/pages/List';
import NewPost from './components/pages/NewPost';
import Profile from './components/pages/Profile';
import EditUser from './components/pages/editUser';
import './css/main.bundle.css';
ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={List} />
      <Route path="/post" component={Post} />
      <Route path="/newpost" component={NewPost} />
      <Route path="/profile" component={Profile} />
      <Route
        path="/edituser"
        component={EditUser}
        render={() => <EditUser email={`Testin@test.co.il`} name={`testing`} />}
      />
    </Switch>
  </BrowserRouter>,
  document.getElementById('root')
);
