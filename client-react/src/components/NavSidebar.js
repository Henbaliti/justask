/* eslint-disable react/display-name, jsx-a11y/click-events-have-key-events */
import { Navigation } from 'react-minimal-side-navigation';
import { useHistory, useLocation } from 'react-router-dom';
import Icon from 'awesome-react-icons';
import React from 'react';

import 'react-minimal-side-navigation/lib/ReactMinimalSideNavigation.css';

export const NavSidebar = () => {
  const history = useHistory();
  const location = useLocation();
  return (
    <React.Fragment>
      {/* Sidebar */}
      <div>
        <Navigation
          activeItemId={location.pathname}
          onSelect={({ itemId }) => {
            history.push(itemId);
          }}
          items={[
            {
              title: 'Home',
              itemId: '/',
              elemBefore: () => <Icon name="coffee" />,
            },
            {
              title: 'About',
              itemId: '/about',
              elemBefore: () => <Icon name="user" />,
              subNav: [
                {
                  title: 'Projects',
                  itemId: '/about/projects',
                },
                {
                  title: 'Members',
                  itemId: '/about/members',
                },
              ],
            },
            {
              title: 'Another Tab',
              itemId: '/another',
              subNav: [
                {
                  title: 'Teams',
                  itemId: '/another/teams',
                },
              ],
            },
          ]}
        />

        <div className="absolute bottom-0 my-8">
          <Navigation
            activeItemId={location.pathname}
            items={[
              {
                title: 'Settings',
                itemId: '/settings',
                elemBefore: () => <Icon name="activity" />,
              },
            ]}
            onSelect={({ itemId }) => {
              history.push(itemId);
            }}
          />
        </div>
      </div>
    </React.Fragment>
  );
};
