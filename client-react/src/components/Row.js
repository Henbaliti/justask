import { Link } from 'react-router-dom';
function Row(props) {
  return (
    <div className="card-body py-3">
      <Link to="/post">
        <div className="row no-gutters align-items-center">
          <div className="col">
            {' '}
            <a href="javascript:void(0)" className="text-big" data-abc="true">
              {props.headLine}
            </a>
            <div className="text-muted small mt-1">
              {props.date} &nbsp;·&nbsp;{' '}
              <a
                href="javascript:void(0)"
                className="text-muted"
                data-abc="true"
              >
                {props.userName}
              </a>
            </div>
          </div>
          <div className="d-none d-md-block col-4">
            <div className="row no-gutters align-items-center">
              <div className="col-4">{props.replies}</div>
              <Link to="/profile">
                <div className="media col-8 align-items-center">
                  {' '}
                  <img
                    src={props.imgSRC}
                    alt=""
                    className="d-block ui-w-30 rounded-circle"
                  />
                  <div className="media-body flex-truncate ml-2">
                    <div className="line-height-1 text-truncate">
                      {props.updateUser}
                    </div>{' '}
                    <a
                      href="javascript:void(0)"
                      className="text-muted small text-truncate"
                      data-abc="true"
                    >
                      by {props.updateDate}
                    </a>
                  </div>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
}
export default Row;
