import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { DashboardLayout } from '../Layout';
function Post(props) {
  return (
    <DashboardLayout>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="card mb-4">
              <div className="card-header">
                <div className="media flex-wrap w-100 align-items-center">
                  {' '}
                  <img
                    src={props.imgSRC}
                    className="d-block ui-w-40 rounded-circle"
                    alt=""
                  />
                  <div className="media-body ml-3">
                    {' '}
                    <a href="javascript:void(0)" data-abc="true">
                      {props.userName}
                    </a>
                    <div className="text-muted small">13 days ago</div>
                  </div>
                  <div className="text-muted small ml-3">
                    <div>
                      Member since <strong>01/1/2019</strong>
                    </div>
                    <div>
                      <strong>134</strong> posts
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <p> {props.content} </p>
              </div>
              <div className="card-footer d-flex flex-wrap justify-content-between align-items-center px-0 pt-0 pb-3">
                <div className="px-4 pt-3">
                  {' '}
                  <a
                    href="javascript:void(0)"
                    className="text-muted d-inline-flex align-items-center align-middle"
                    data-abc="true"
                  >
                    {' '}
                    <i className="fa fa-heart text-danger" />
                    &nbsp; <span className="align-middle">445</span>{' '}
                  </a>{' '}
                  <span className="text-muted d-inline-flex align-items-center align-middle ml-4">
                    {' '}
                    <i className="fa fa-eye text-muted fsize-3" />
                    &nbsp; <span className="align-middle">14532</span>{' '}
                  </span>{' '}
                </div>
                <div className="px-4 pt-3">
                  {' '}
                  <button type="button" className="btn btn-primary">
                    <i className="ion ion-md-create" />
                    &nbsp; Reply
                  </button>{' '}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </DashboardLayout>
  );
}

export default Post;
