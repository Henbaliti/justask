import { Link } from 'react-router-dom';
import Row from '../Row';
import { DashboardLayout } from '../Layout';
function List() {
  return (
    <DashboardLayout>
      <div className="container-fluid">
        <div className="card mb-3">
          <div className="card-header pl-0 pr-0">
            <div className="row no-gutters w-100 align-items-center">
              <div className="col ml-3">Topics</div>
              <div className="col-4 text-muted">
                <div className="row no-gutters align-items-center">
                  <div className="col-4">Replies</div>
                  <div className="col-8">Last update</div>
                </div>
              </div>
            </div>
          </div>

          <Row
            userName="Hen"
            headLine="Stam subject 1"
            date="01/02/2020"
            replies="67"
            updateDate="4 days ago"
            updateUser="Hen"
            imgSRC="https://res.cloudinary.com/dxfq3iotg/image/upload/v1574583246/AAA/2.jpg"
          />
          <Row
            userName="Yariv"
            headLine="Stam subject 2"
            date="22/08/1995"
            replies="7"
            updateDate="4 days ago"
            updateUser="Hen"
            imgSRC="https://res.cloudinary.com/dxfq3iotg/image/upload/v1574583246/AAA/2.jpg"
          />
          <Row
            userName="Afiik"
            headLine="Stam subject 3"
            date="12/12/1996"
            replies="203"
            updateDate="1 days ago"
            updateUser="Yarin"
            imgSRC="https://res.cloudinary.com/dxfq3iotg/image/upload/v1574583246/AAA/2.jpg"
          />

          <hr className="m-0" />
        </div>
        <nav>
          <ul className="pagination mb-5">
            <li className="page-item disabled">
              <a
                className="page-link"
                href="javascript:void(0)"
                data-abc="true"
              >
                «
              </a>
            </li>
            <li className="page-item active">
              <a
                className="page-link"
                href="javascript:void(0)"
                data-abc="true"
              >
                1
              </a>
            </li>
            <li className="page-item">
              <a
                className="page-link"
                href="javascript:void(0)"
                data-abc="true"
              >
                2
              </a>
            </li>
            <li className="page-item">
              <a
                className="page-link"
                href="javascript:void(0)"
                data-abc="true"
              >
                3
              </a>
            </li>
            <li className="page-item">
              <a
                className="page-link"
                href="javascript:void(0)"
                data-abc="true"
              >
                »
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </DashboardLayout>
  );
}

export default List;
