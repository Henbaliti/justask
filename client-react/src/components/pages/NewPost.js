import { Form, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { DashboardLayout } from '../Layout';

function NewPost() {
  const handleSubmit = (event) => {
    const target = event.target;

    // Question q = new Question(traget.formHeadline,traget.formContent);
  };
  return (
    <DashboardLayout>
      <Form onSubmit={handleSubmit}>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="card mb-4">
                <div className="card-header">
                  <div className="media flex-wrap w-100 align-items-center">
                    {' '}
                    <img
                      src="#"
                      className="d-block ui-w-40 rounded-circle"
                      alt=""
                    />
                    <div className="media-body ml-3">
                      {' '}
                      <a href="javascript:void(0)" data-abc="true">
                        yariv245
                      </a>
                      <div className="text-muted small">13 days ago</div>
                    </div>
                    <div className="text-muted small ml-3">
                      <div>
                        Member since <strong>01/1/2019</strong>
                      </div>
                      <div>
                        <strong>134</strong> posts
                      </div>
                    </div>
                  </div>
                </div>
                <div className="card-body">
                  <Form.Group controlId="formHeadline">
                    <Form.Label>Headline</Form.Label>
                    <Form.Control type="text" placeholder="Enter Headline" />
                  </Form.Group>
                  <Form.Group controlId="formContent">
                    <Form.Label>Content</Form.Label>
                    <Form.Control as="textarea" rows={3} />
                  </Form.Group>
                </div>
                <div className="card-footer d-flex flex-wrap justify-content-between align-items-center px-0 pt-0 pb-3">
                  <div className="px-4 pt-3">
                    {' '}
                    <a
                      href="javascript:void(0)"
                      className="text-muted d-inline-flex align-items-center align-middle"
                      data-abc="true"
                    >
                      {' '}
                      <i className="fa fa-heart text-danger" />
                      &nbsp;{' '}
                      <span className="align-middle">
                        Created by Yariv Menachem
                      </span>{' '}
                    </a>{' '}
                  </div>
                  <div className="px-4 pt-3">
                    {' '}
                    <Button variant="primary" type="submit">
                      Submit
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Form>
    </DashboardLayout>
  );
}

export default NewPost;
