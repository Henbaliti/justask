import React from 'react';
import { Link } from 'react-router-dom';
import { NavSidebar } from './NavSidebar';
import BodyWrapper from './BodyWrapper';

export const DashboardLayout = ({ children }) => {
  return (
    <BodyWrapper>
      <div className="flex h-screen bg-gray-200">
        <NavSidebar />

        <div className="flex flex-col flex-1 overflow-hidden">
          <main className="content">
            <section className="sm:flex-row flex flex-col flex-1">
              <div
                className="content-box"
                style={{ flexGrow: 2, flexBasis: '0%' }}
              >
                <div className="d-flex flex-wrap justify-content-between mt-100">
                  <div>
                    {' '}
                    <Link to="/newpost">
                      <button
                        type="button"
                        className="btn btn-shadow btn-wide btn-primary"
                      >
                        {' '}
                        <span className="btn-icon-wrapper pr-2 opacity-7">
                          {' '}
                          <i className="fa fa-plus fa-w-20" />{' '}
                        </span>{' '}
                        New thread{' '}
                      </button>{' '}
                    </Link>
                  </div>
                  <div className="col-12 col-md-3 p-0 mb-3">
                    {' '}
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Search..."
                    />{' '}
                  </div>
                </div>
                {children}
              </div>
            </section>
          </main>
        </div>
      </div>
    </BodyWrapper>
  );
};
